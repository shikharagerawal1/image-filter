Github Link for the image-filter project:- https://gitlab.com/shikharagerawal1/image-filter

Elastic beanstalk endpoint URL:- http://udagram-agrawal-imagefilter-dev.us-east-1.elasticbeanstalk.com

Filtered Image URL :- http://udagram-agrawal-imagefilter-dev.us-east-1.elasticbeanstalk.com/filteredimage?image_url=https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/1200px-Cat03.jpg

Please Note:- the image link you had provided for testing is removed from the server, so I found the above link for testing.